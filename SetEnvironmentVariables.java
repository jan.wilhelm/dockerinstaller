import java.io.*;
import java.util.Scanner;

public class SetEnvironmentVariables {

    private static final String LAST_DIRECTORY_FILE = "last_directory.txt";

    public static String readLastDirectory() {
        try {
            File file = new File(LAST_DIRECTORY_FILE);
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String lastDirectory = reader.readLine();
                reader.close();
                return lastDirectory;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeLastDirectory(String directory) {
        try {
            FileWriter writer = new FileWriter(LAST_DIRECTORY_FILE);
            writer.write(directory);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // Set environment variables as system properties
        System.setProperty("API_URL", "http://localhost:3000");
        System.setProperty("IO_URL", "http://localhost:80");
        System.setProperty("POSTMARK_API_KEY", "blank");
        System.setProperty("FROM_SENDER", "stg@github.io");

        // Read last directory used
        String lastDirectory = readLastDirectory();

        // Prompt for directory
        Scanner scanner = new Scanner(System.in);
        String rootDirectory;
        boolean useLastDirectory = false;

        while (true) {
            System.out.print("Use the last directory (" + lastDirectory + ")? (Y/N): ");
            String choice = scanner.nextLine();
            if (choice.equalsIgnoreCase("Y")) {
                if (lastDirectory != null) {
                    rootDirectory = lastDirectory;
                    useLastDirectory = true;
                    break;
                } else {
                    System.out.println("No last directory found.");
                }
            } else if (choice.equalsIgnoreCase("N")) {
                System.out.print("Enter the root directory for running the Docker command: ");
                rootDirectory = scanner.nextLine();
                writeLastDirectory(rootDirectory);
                break;
            } else {
                System.out.println("Invalid choice. Please enter Y or N.");
            }
        }

        // Run Docker command
        if (useLastDirectory) {
            System.out.println("Using last directory: " + lastDirectory);
        } else {
            System.out.println("Using directory: " + rootDirectory);
        }

        String dockerCommand = "docker-compose up -d --force-recreate --renew-anon-volumes";
        try {
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("bash", "-c", dockerCommand);
            processBuilder.directory(new File(rootDirectory));
            Process process = processBuilder.start();

            // Capture error output
            InputStream errorStream = process.getErrorStream();
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(errorStream));
            StringBuilder errorOutput = new StringBuilder();
            String line;
            while ((line = errorReader.readLine()) != null) {
                errorOutput.append(line).append("\n");
            }
            errorReader.close();

            int exitCode = process.waitFor();
            if (exitCode == 0) {
                System.out.println("Docker command executed successfully.");
            } else {
                System.out.println("Error executing Docker command. Details:\n" + errorOutput.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
